﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AVTKProjekt.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using AVTKProjekt.Database;
using System.Threading;

namespace AVTKProjekt.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly AVTKContext _db;

        public HomeController(ILogger<HomeController> logger, AVTKContext db)
        {
            _logger = logger;
            _db = db;
            var nfInfo = new System.Globalization.CultureInfo("cs", false)
            {
                NumberFormat = {
                    NumberDecimalSeparator = "."
                }
            };
            Thread.CurrentThread.CurrentCulture = nfInfo;
            Thread.CurrentThread.CurrentUICulture = nfInfo;
        }


        [HttpPost]
        public IActionResult SetIntervals(IntervalModel intervalModel)
        {
            var interval = _db.Intervals.FirstOrDefault();
            if (intervalModel.FromInterval < intervalModel.ToInterval) // from nesmi byt vetsi nez to
            {
                interval.FromInterval = intervalModel.FromInterval;
                interval.ToInterval = intervalModel.ToInterval;

                _db.SaveChanges();
            }

            return RedirectToAction("Index");
        }
        [HttpGet("change-state")]
        public IActionResult ChangeLightState([FromQuery]bool state) {
            var x = _db.Light.FirstOrDefault();
            if (x != null)
            {
                x.LightState = state;
            }
            else
            {
                x = new Light();
                x.LightState = state;
                _db.Add(x);

            }
            _db.SaveChanges();
            return RedirectToAction("Index");

        }

        public void GetValueFromDB(MeasurementModel measurementModel)
        {
            var x = _db.Measurements.Select(o=>o.Value).ToList();
            string values = string.Join(",", x.ToArray());
            measurementModel.DisplayValue = values;
        }
        public void GetTimeFromDB(MeasurementModel measurementModel) {
            var month = _db.Measurements
                .OrderByDescending(o =>o.TimeValue)
                .Select(o => o.TimeValue.ToString("dd MM HH:mm:ss")).Take(10).ToList();
            measurementModel.DisplayTimeMonth = "'" + string.Join("','", month.ToArray())+ "'";
        }

       public LightModel GetLightStateFromDB()
        {
            var lightModel = new LightModel();
            var x = _db.Light.Select(o => o.LightState).FirstOrDefault();
            lightModel.LightState = x;

            return lightModel;
        }

        public IActionResult Index()
        {
            var model = GetMeasurementModel();
            GetTimeFromDB(model);
            GetValueFromDB(model);
            return View("Index", (model, GetIntervalModel(), GetLightStateFromDB()));
        }
        private MeasurementModel GetMeasurementModel()
        {
            var measurement = _db.Measurements.OrderByDescending(o => o.Id).FirstOrDefault();

            if (measurement == null)
            {
                return new MeasurementModel
                {
                    Value = 0,
                    TimeValue = DateTime.Now,
            };
            }

            return new MeasurementModel
            {
                Value = measurement.Value,
                TimeValue = measurement.TimeValue,
            };
        }

        private IntervalModel GetIntervalModel()
        {
            var interval = _db.Intervals.FirstOrDefault();
            if (interval == null)
            {
                interval = new Interval
                {
                    FromInterval = 2.5,
                    ToInterval = 5.5
                };
                _db.Add(interval);
                _db.SaveChanges();
            }

            return new IntervalModel
            {
                FromInterval = interval.FromInterval,
                ToInterval = interval.ToInterval
            };
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        
    }
}
