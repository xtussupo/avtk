﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVTKProjekt.Database;
using AVTKProjekt.Models;
using AVTKProjekt.Models.API;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AVTKProjekt.Controllers
{
    [Route("api/measurement")]
    [ApiController]
    public class MyAPIController : ControllerBase
    {
        private AVTKContext db;
        public MyAPIController(AVTKContext db) {
            this.db = db;
        }

        [HttpGet]

        public ActionResult<LightModel> SendDataToRaspberry() {
            var x = db.
                Light.Select(o => o.LightState).FirstOrDefault();
            return new LightModel
            {
                LightState = x
            };
        }


        [HttpPost]
        public IActionResult GetDataFromRaspberry([FromBody]FromRaspberry mydata) {
            var measurement = new Measurement();
            measurement.Value = mydata.measurement;
            measurement.TimeValue = mydata.recordedTime;
            db.Measurements.Add(measurement);
            db.SaveChanges();
            return Ok();
        }

      
    }




}