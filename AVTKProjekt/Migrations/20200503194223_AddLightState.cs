﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AVTKProjekt.Migrations
{
    public partial class AddLightState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "LightState",
                table: "Measurements",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LightState",
                table: "Measurements");
        }
    }
}
