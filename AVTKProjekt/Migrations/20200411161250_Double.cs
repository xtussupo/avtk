﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AVTKProjekt.Migrations
{
    public partial class Double : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "ToInterval",
                table: "Intervals",
                nullable: false,
                oldClrType: typeof(float),
                oldType: "float");

            migrationBuilder.AlterColumn<double>(
                name: "FromInterval",
                table: "Intervals",
                nullable: false,
                oldClrType: typeof(float),
                oldType: "float");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "ToInterval",
                table: "Intervals",
                type: "float",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<float>(
                name: "FromInterval",
                table: "Intervals",
                type: "float",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}
