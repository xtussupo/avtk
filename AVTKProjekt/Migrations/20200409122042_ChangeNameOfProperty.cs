﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AVTKProjekt.Migrations
{
    public partial class ChangeNameOfProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Time",
                table: "Measurments");

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeValue",
                table: "Measurments",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimeValue",
                table: "Measurments");

            migrationBuilder.AddColumn<DateTime>(
                name: "Time",
                table: "Measurments",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
