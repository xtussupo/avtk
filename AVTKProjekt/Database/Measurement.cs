﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVTKProjekt.Database
{
    public class Measurement
    {
        public int Id { get; set; }
        public float Value { get; set; } //hodnota, kterou chceme ukladat
        public DateTime TimeValue { get; set; } //cas zaznamenani
    }
}
