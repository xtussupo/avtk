﻿using System.ComponentModel.DataAnnotations;

namespace AVTKProjekt.Database
{
    public class Interval
    {
        [Key]
        public int IntervalId { get; set; }
        public double FromInterval { get; set; }
        public double ToInterval { get; set; }
    }
}
