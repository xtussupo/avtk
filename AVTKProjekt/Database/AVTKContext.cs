﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace AVTKProjekt.Database
{
    public class AVTKContext : DbContext
    {
        public AVTKContext()
        {}

        public AVTKContext(DbContextOptions<AVTKContext> options): base(options){ }

        public DbSet<Measurement> Measurements {get; set;}
        public DbSet<Interval> Intervals { get; set; }
        public DbSet<Light> Light { get; set; }


    }
}
