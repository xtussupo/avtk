﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVTKProjekt.Database
{
    public class Light
    {
        public int Id { get; set; }
        public bool LightState { get; set; }  //stav ziarovky

    }
}
