﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AVTKProjekt.Models
{
    public class IntervalModel
    {
        [Required]
        public double FromInterval { get; set; }

        [Required]
        public double ToInterval { get; set; }
    }
}
