﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;

namespace AVTKProjekt.Models
{
    public class MeasurementModel 
    {
        public float Value { get; set; }
        public DateTime TimeValue { get; set; }
        public string DisplayValue { get; set; }
        public string DisplayTimeMonth { get; set; }
       
    }
}
