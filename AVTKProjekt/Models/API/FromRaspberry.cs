﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVTKProjekt.Models.API
{
    public class FromRaspberry
    {
        public DateTime recordedTime { get; set; }
        public float measurement { get; set; }
        public  bool lightState { get; set; }
        
    }
}
